def reverse_list(lst):
    return lst[::-1]

assert reverse_list([1, 2, 3]) == [3, 2, 1]
