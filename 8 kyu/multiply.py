def multiply(a, b):
    return a * b

assert multiply(3, 4) == 12
assert multiply(10, 10) == 100
