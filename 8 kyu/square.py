def square(n):
    return n ** 2


assert square(2) == 4
assert square(50) == 2500
assert square(1) == 1
