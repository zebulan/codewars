def doubleInteger(i):
    """ double_integer == PEP8 (forced mixedcase by CodeWars) """
    return i * 2

assert doubleInteger(2) == 4
