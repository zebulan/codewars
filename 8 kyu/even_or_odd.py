def even_or_odd(number):
    return ('Even', 'Odd')[number % 2]
    # return 'Even' if number % 2 == 0 else 'Odd'

assert even_or_odd(2) == 'Even'
assert even_or_odd(11) == 'Odd'
