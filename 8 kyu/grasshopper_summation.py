def summation(num):
    return sum(xrange(1, num + 1))

assert summation(1) == 1
assert summation(8) == 36
