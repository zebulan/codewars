def bool_to_word(bool):
    return 'YES' if bool else 'NO'

assert bool_to_word(True) == 'YES'
assert bool_to_word(False) == 'NO'
