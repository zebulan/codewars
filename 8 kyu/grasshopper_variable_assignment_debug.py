a = "dev"
b = "Lab"
name = a + b

assert a == 'dev'
assert b == 'Lab'
assert name == 'devLab'
