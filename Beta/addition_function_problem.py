from operator import add

assert add(1, 3) == 4
assert add(2, 4) == 6
