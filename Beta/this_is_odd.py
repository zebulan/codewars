def is_odd(n):
    return n % 2 == 1

assert is_odd(5) is True
assert is_odd(4) is False
assert is_odd(3.0) is True
assert is_odd(-1) is True
