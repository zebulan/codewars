from collections import Counter as count

assert count(['a', 'a', 'b', 'b', 'b']), {'a': 2, 'b': 3}
