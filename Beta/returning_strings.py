def greet(name):
    return 'Hello, {} how are you doing today?'.format(name)

assert greet('Ryan') == 'Hello, Ryan how are you doing today?'
