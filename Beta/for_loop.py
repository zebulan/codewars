def problem(n):
    return range(1, n + 1)

assert problem(5) == [1, 2, 3, 4, 5]
