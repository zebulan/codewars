def divisible_by_four(num):
    return num % 4 == 0

assert divisible_by_four(12) is True
assert divisible_by_four(7) is False
