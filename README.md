# CodeWars

My solutions for CodeWars problems are written using Python 2.7.10

[zebulan - CodeWars Profile](http://www.codewars.com/users/zebulan)

* 8 - 7 kyu: Beginner
* 6 - 5 kyu: Novice
* 4 - 3 kyu: Competent
* 2 - 1 kyu: Proficient
* 1 - 5 dan: Expert
* 5 - 8 dan: Master

Problems are separated into appropriate folders according to difficulty.
