def findSmallestInt(arr):
    """ find_smallest_int == PEP8 (forced mixedCase by CodeWars) """
    return min(arr)

assert findSmallestInt([78, 56, 232, 12, 11, 43]) == 11
assert findSmallestInt([78, 56, -2, 12, 8, -33]) == -33
