def penultimate(nums):
    return nums[-2]

assert penultimate([1, 2, 3, 4]) == 3
assert penultimate([1, 2, 3, 4, 5, 6, 7]) == 6
