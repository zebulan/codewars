def swap(string):
    return string.swapcase()

assert swap('HelloWorld') == 'hELLOwORLD'
assert swap('CodeWars') == 'cODEwARS'
