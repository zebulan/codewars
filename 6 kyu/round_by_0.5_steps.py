def solution(n):
    return round(n * 2) / 2.0

assert solution(4.2) == 4
assert solution(4.25) == 4.5
assert solution(4.4) == 4.5
assert solution(4.6) == 4.5
assert solution(4.75) == 5
assert solution(4.8) == 5
